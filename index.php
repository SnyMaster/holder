<?php
declare(strict_types = 1);

use lib\Holder;
use lib\Employee;
use lib\animal\Animal;
use lib\animal\AnimalOwnersCats;
use lib\animal\AnimalOwnersTurtles;
use lib\animal\AnimalOwnersDogs;

require 'lib/autoloader.php';

/**
 * @var animal Animal
 */
$holder = new Holder();
$employee = new Employee($holder);

// добавление животных в приют
$employee->doWork(new lib\work\TakeNew($holder, (new AnimalOwnersCats('Masha', 15))->giveOut()));
$employee->doWork(new lib\work\TakeNew($holder, (new AnimalOwnersDogs('Tuzya', 25))->giveOut()));
$employee->doWork(new lib\work\TakeNew($holder, (new AnimalOwnersTurtles('Alisa', 500))->giveOut()));
$employee->doWork(new lib\work\TakeNew($holder, (new AnimalOwnersCats('Vasiay', 2))->giveOut()));
$employee->doWork(new lib\work\TakeNew($holder, (new AnimalOwnersDogs('Ada', 45))->giveOut()));
$employee->doWork(new lib\work\TakeNew($holder, (new AnimalOwnersDogs('Sharick', 35))->giveOut()));
$employee->doWork(new lib\work\TakeNew($holder, (new AnimalOwnersCats('Kisa', 20))->giveOut()));
$employee->doWork(new lib\work\TakeNew($holder, (new AnimalOwnersTurtles('Mery', 10))->giveOut()));

// общий список
$list = $employee->doWork(new lib\work\ListAll($holder));
echo 'В приюте ', count($list), ' животных:', '<br/>';
array_map(function(Animal $animal)
    { 
        echo $animal->getTypeName(), ' ему ', $animal->ageFormated, ' и его зовут ', $animal->name, '<br/>'; 
    }, $list);
echo '<br />';

// список животных определённого типа отсортированный по имени
$list = $employee->doWork(new lib\work\SortByNameWork($holder, Animal::TYPE_TURTLE));
echo 'В приюте ', count($list), ' черепахинских:', '<br/>';
array_map(function(Animal $animal)
    { 
        echo $animal->getTypeName(), ' ему ', $animal->ageFormated, ' и его зовут ', $animal->name, '<br/>'; 
    }, $list);
echo '<br />';

// список животных определённого типа отсортированный по имени
$list = $employee->doWork(new lib\work\SortByNameWork($holder, Animal::TYPE_DOG));
echo 'В приюте ', count($list), ' собакенций:', '<br/>';
array_map(function(Animal $animal)
    { 
        echo $animal->getTypeName(), ' ему ', $animal->ageFormated, ' и его зовут ', $animal->name, '<br/>'; 
    }, $list);
echo '<br />';

// список животных определённого типа отсортированный по имени
$list = $employee->doWork(new lib\work\SortByNameWork($holder, Animal::TYPE_CAT));
echo 'В приюте ', count($list), ' котейков:', '<br/>';
array_map(function(Animal $animal)
    { 
        echo $animal->getTypeName(), ' ему ', $animal->ageFormated, ' и его зовут ', $animal->name, '<br/>'; 
    }, $list);
echo '<br />';
    
// передача животных
// создание нового холдера
$newOwner = new Holder();
try {
    $employee->doWork((new lib\work\GiveOutToOwner($holder, $newOwner, 'max')));
} catch (\InvalidArgumentException $ex) {
    echo $ex->getMessage(), '<br/>';
}

echo 'У меня есть : </br>';
array_map(function(Animal $animal)
    { 
        echo $animal->getTypeName(), ' ему ', $animal->ageFormated, ' его зовут ', $animal->name, '<br/>'; 
    }, $newOwner->all());
    
try {    
    $employee->doWork((new lib\work\GiveOutToOwner($holder, $newOwner, 'maxim')));
} catch (\InvalidArgumentException $ex) {
    echo 'Ошибка передачи животного из приюта:';
    echo $ex->getMessage(), '<br/>';
}

try {    
    $employee->doWork((new lib\work\GiveOutToOwner($holder, $newOwner, 'max', Animal::TYPE_DOG)));
    $employee->doWork((new lib\work\GiveOutToOwner($holder, $newOwner, 'max', Animal::TYPE_CAT)));
} catch (\InvalidArgumentException $ex) {
    echo 'Ошибка передачи животного из приюта:';
    echo $ex->getMessage(), '<br/>';
}
echo '<br />';
echo 'А теперрь к меня есть:</br>';
array_map(function(Animal $animal)
    { 
        echo $animal->getTypeName(), ' ему ', $animal->ageFormated, ' его зовут ', $animal->name, '<br/>'; 
    }, $newOwner->all());
    
echo '<br />';

$list = $employee->doWork(new lib\work\ListAll($holder));
echo 'В приюте осталось ', count($list), ' животных:', '<br/>';
array_map(function(Animal $animal)
    { 
        echo $animal->getTypeName(), ' ему ', $animal->ageFormated, ' и его зовут ', $animal->name, '<br/>'; 
    }, $list);
echo '<br />';



