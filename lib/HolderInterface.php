<?php

namespace lib;

use lib\animal\Animal;

interface HolderInterface
{
    public function all() : array;
    public function findById(string $id) : ?Animal;
    public function add(Animal $animal) : ?string;
    public function delete(string $id) : bool;
}
