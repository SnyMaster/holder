<?php 

namespace lib\animal;

/**
 * @property string $name
 * @property integer $age Возраст в месяцах
 * @property string $ageFormated формматированное представление возраста
 * @property string  $type тип животного 
 */

abstract  class Animal
{
    const TYPE_CAT = 'cat';
    const TYPE_DOG = 'dog';
    CONST TYPE_TURTLE = 'turtle';

    protected $name;
    protected $age;
    
    public function __construct(string $name, int $age)
    {
        if ($age <= 0) {
            throw new \InvalidArgumentException("Возраст животного не может быть равен или меньше 0");
        }
        if (trim($name) === '') {
            throw new \InvalidArgumentException("Имя животного не может быть пустой строкой");
        }
        $this->name = $name;
        $this->age = $age;
    }
    
    public function __get($name) {
        if ($name === 'type') {
            return $this->getType();
        } elseif ($name === 'ageFormated') {
            return $this->getAgeFormated();
        } elseif (isset($this->$name)) {
            return $this->$name;
        }
    }
    
    final public function getAgeFormated()
    {
        return implode(' и ', array_filter([$this->ageYearsFormatted(), $this->ageMonthFormatted()]));
    }
    
    final public function getTypeName() : ?string
    {
        $animalClasses = [
            Animal::TYPE_DOG => 'Собака',
            Animal::TYPE_CAT => 'Кот',
            Animal::TYPE_TURTLE => 'Черепаха'
        ];
        
        return array_key_exists($this->type, $animalClasses) ?
                $animalClasses[$this->type] : null;
        
    }
    
    final protected function ageYearsFormatted()
    {
        $years = floor($this->age / 12);
        $nameYears =[
            1 => 'год', 2 => 'года', 3 => 'года', 4 => 'года'
        ];
        
        return $years ? 
                ($years . ' ' .  (isset($nameYears[$years]) ? $nameYears[$years] : 'лет')) : null;
    }
    
    final protected function ageMonthFormatted()
    {
        $month = $this->age - (floor($this->age / 12)) * 12;
        $nameMonth = [
            1 => 'месяц',
            2 => 'месяца',
            3 => 'месяца',
            4 => 'месяца'
        ];
        
        return $month ? 
                ($month . ' ' . (isset($nameMonth[$month]) ? $nameMonth[$month] : 'месяцев')) : null;
        
    }
    
    abstract public function getType() : string;
    
}