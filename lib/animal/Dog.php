<?php

namespace lib\animal;

use lib\animal\Animal;

class Dog extends Animal
{
    public function getType() : string
    {
        return Animal::TYPE_DOG;
    }
}

