<?php

namespace lib\animal;

use lib\animal\Animal;

class Cat extends Animal
{
    public function getType() : string
    {
        return Animal::TYPE_CAT;
    }
}
