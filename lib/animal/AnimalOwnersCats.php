<?php

namespace lib\animal;

use lib\animal\AnimalOwners;
use lib\animal\Animal;
use lib\animal\Cat;

/**
 * Владелец КОТА
 * 
 * @return Animal|null
 */

class AnimalOwnersCats extends AnimalOwners
{
   /**
    * Отдаёт кота
    * 
    * @return Animal
    */
    public function giveOut() : Animal
    {
        return new Cat($this->name, $this->age);
    }
}
