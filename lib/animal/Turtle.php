<?php

namespace lib\animal;

use lib\animal\Animal;

class Turtle extends Animal
{
    public function getType() : string
    {
        return Animal::TYPE_TURTLE;
    }
}
