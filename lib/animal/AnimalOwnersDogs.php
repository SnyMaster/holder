<?php

namespace lib\animal;

use lib\animal\AnimalOwners;
use lib\animal\Animal;
use lib\animal\Dog;

/**
 * Владелец животного СОБАКА
 */

class AnimalOwnersDogs extends AnimalOwners
{
    
   /**
    * Отдаёт собаку
    * 
    * @return Animal
    */
    public function giveOut() : Animal
    {
        return new Dog($this->name, $this->age);
    }
}
