<?php

namespace lib\animal;

use lib\animal\Animal;

abstract class AnimalOwners
{
  
    /**
     * Владелец животного
     * 
     * @param string $type тип животного
     * @param string $name кличка
     * @param int $age возраст
     * 
     * @return Animal|null
     */
    protected $name;
    protected $age;
    
    public function __construct(string $name, int $age)
    {
        if ($age <= 0) {
            throw new \InvalidArgumentException("Возраст животного не может быть равен или меньше 0");
        }
        if (trim($name) === '') {
            throw new \InvalidArgumentException("Имя животного не может быть пустой строкой");
        }
        $this->name = $name;
        $this->age = $age;
    }
    
    abstract public function giveOut() : Animal;
}
