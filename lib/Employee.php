<?php

namespace lib;
use lib\work\Work;

/**
 * Сотрудник приюта (любит работать, но нужны инструкции, сам ничего не знает)
 */
class Employee
{
    
    public function doWork(Work $work)
    {
        return $work->execute();
    }
    
}
