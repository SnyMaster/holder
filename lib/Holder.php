<?php

namespace lib;

use lib\HolderInterface;
use lib\animal\Animal;
use \Exception;

/**
 * Хранилище (приют или владелец)
 */
class Holder implements HolderInterface
{
    private $list = [];
    
    public function all() : Array
    {
        return $this->list;
    }
    
    public function findById(string $id) : ?Animal
    {
        return array_key_exists($id, $this->list) ? $this->list[$id] : null;
    }
    
    public function add(Animal $animal) : ?string
    {
        try {
            list($micsec, $sec) = explode(" ", microtime());
            $id = $sec . '.' . explode(".", $micsec)[1];
            $this->list[$id] = $animal;
            return array_key_last($this->list);
        } catch (Exception $ex) {
            return null;
        }
    }
    
    public function delete(string $id) : bool
    {
        if (isset($this->list[$id])) {
            unset($this->list[$id]);
            return true;
        }
        return false;
    }
}
