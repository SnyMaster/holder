<?php

namespace lib\work;

use lib\work\Work;
use lib\Holder;
use lib\animal\Animal;

class TakeNew extends Work
{
    /**
     *
     * @var Holder $holder 
     * @var Animal $animal
     */
    private $animal;
    
    public function __construct(Holder $holder, Animal $animal) 
    {
        parent::__construct($holder);
        $this->animal = $animal;
    }
    
    public function execute() : string
    {
        return $this->holder->add($this->animal);
    }
}
