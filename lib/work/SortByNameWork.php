<?php

namespace lib\work;

use lib\work\Work;
use lib\animal\Animal;
use lib\Holder;

/**
 * Инструкция для работника как сортировать список по кличке в порядке возрастания с возможность оотфильтровать по типу и имени
 * 
 * @property string $filterByType
 * @property string $filterByName
 */
class SortByNameWork extends Work
{
    use Listing;
    
    
    public function __construct(Holder $holder, string $filterByType = null, string $filterByName = null)
    {    
        parent::__construct($holder);
        $this->filterByType = $filterByType;
        $this->filterByName = $filterByName;
    }
    
    /**
     * Выполняет задачу сортировки переданного списка животных по значению поля имя в порядке возрастания
     * 
     * @param Animal[] $listAnimals
     * @return Animal[]
     */
    public function execute(): array {       
        $listAnimals = $this->list();
        usort($listAnimals, function(Animal $a1, Animal $a2) { return strcmp($a1->name, $a2->name); });
        return $listAnimals;
    }
}
