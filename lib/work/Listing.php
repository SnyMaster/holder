<?php

namespace lib\work;

use lib\animal\Animal;

/**
 * Список животных в приюте с возможностью фильтрации 
 * 
 * 
 * @property string $filterByType
 * @property string $filterByName
 * 
 */trait Listing
{
    public $filterByType;
    public $filterByName;
    
    public function list()
    {
        try {
            $holder = $this->getHolder();
        } catch (\Exception $ex) {
            throw new ErrorException('Не удалось получить доступ к Holder объекту в методе "all"');
        }
        $listAnimals = $holder->all();
        return ($this->filterByType || $this->filterByName) ?
            array_filter($listAnimals, 
                    function(Animal $animal) 
                    { 
                        return (!$this->filterByName || ($this->filterByName && $animal->name === $this->filterByName)) &&
                                (!$this->filterByType || ($this->filterByType && $animal->type === $this->filterByType)); 
                    }) :
            $listAnimals;
        
    }
}
