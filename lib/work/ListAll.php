<?php

namespace lib\work;

use lib\work\Work;
use lib\Holder;

/**
 *  Список животных в приюте с возможностью фильтрации 
 * 
 * @property string $filterByType
 * @property string $filterByName
 * 
 */
class ListAll extends Work
{
    use Listing;
   
    /**
     * 
     * @param Holder $holder 
     * @param string $filterByType  задаёт фильтр по типу
     * @param string $filterByName задаёт фильтр по имени
     */
    public function __construct(Holder $holder, string $filterByType = null, string $filterByName = null)
    {   
        parent::__construct($holder);
        $this->filterByName = $filterByName;
        $this->filterByType = $filterByType;
    }
    
    public function execute(): array 
    {          
        return $this->list();
    }
}
