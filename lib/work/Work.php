<?php

namespace lib\work;

use lib\Holder;

/**
 * Инструкции для работника приюта, без них работа не делается
 * 
 */
abstract class Work
{
    protected $holder;
    
    public function __construct(Holder $holder) 
    {
        $this->holder = $holder;
    }
    
    public function getHolder()
    {
        return $this->holder;
    }
    
    abstract public function execute();    
    
}