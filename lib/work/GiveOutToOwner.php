<?php

namespace lib\work;

use lib\work\Work;
use lib\Holder;

/**
 * Инструкция по передаче животоного из приюта новому владельцу
 * 
 */
class GiveOutToOwner extends Work
{
    use Listing;
    
    private $holderTo;
    private $timeInHolder;
    
    /**
     * 
     * @param Holder $holderFrom хранилище источник
     * @param Holder $holderTo хранилище куда переётся еденица из хранилища источникаа 
     * @param string $timeInHolder  указывает какую еденицу выбирать для передачи из источника , 
     *                  max - с максимальным временем пребывания в хранилище-истонике, 
     *                  min - с минимальным временем пребывания в хранилище-истонике
     * @param string $filterByType задаёт фильтр по типу
     * @param string $filterByName задаёт фильтр по имени
     * 
     * @throws \InvalidArgumentException
     */
    public function __construct(Holder $holderFrom, Holder $holderTo, string $timeInHolder = 'max', string $filterByType = null, string $filterByName = null) 
    {
        parent::__construct($holderFrom);
        $this->holderTo = $holderTo;
        $this->filterByName = $filterByName;
        $this->filterByType = $filterByType;
        
        if (!in_array($timeInHolder, ['min', 'max'])) {
            throw new \InvalidArgumentException('Параметр "timeInHolder" может принимать значения "min" или "max"');
        }
        $this->timeInHolder = $timeInHolder;

    }
    public function execute() : string {
        $list = $this->list();
        if ($this->timeInHolder === 'max') {
            krsort($list);
        } else {
            ksort($list);
        }
        $id = array_key_last($list);
        $animal = array_pop($list);
        $this->holder->delete($id);
        return $this->holderTo->add($animal);
    }
}