<?php
declare (strict_types = 1);

use PHPUnit\Framework\TestCase;
use lib\Holder;
use lib\animal\AnimalOwnersCats;
use lib\animal\AnimalOwnersDogs;
use lib\animal\AnimalOwnersTurtles;
use lib\Employee;

class ClassesTest extends TestCase
{
 
    protected $holder;
    protected $employee;
    protected $animalOwners;
       
    public function testCreateHolder()
    {
        $holder = new Holder();
        $this->assertIsObject($holder);
        $this->assertInstanceOf(lib\HolderInterface::class, $holder);        
        return $holder;
    }
    
    /**
     * 
     * @depends testCreateHolder
     */
    public function testCreateEmployee($holder)
    {
        $employee = new Employee($holder);
        $this->assertIsObject($employee);        
        return $employee;
    }
    
    public function testCreateAnimalOwnersCats()
    {
        $animalOwners = new AnimalOwnersCats('Miss', 14);
        $this->assertIsObject($animalOwners);
        return $animalOwners;
    }
    
    public function testCreateAnimalOwnersDogs()
    {
        $animalOwners = new AnimalOwnersDogs('Alf', 10);
        $this->assertIsObject($animalOwners);
        return $animalOwners;
    }
    
    public function testCreateAnimalOwnersTurtles()
    {
        $animalOwners = new AnimalOwnersTurtles('Ferrary', 600);
        $this->assertIsObject($animalOwners);
        return $animalOwners;
    }
    
    /**
     * @depends testCreateAnimalOwnersCats
     * @depends testCreateAnimalOwnersDogs
     * @depends testCreateAnimalOwnersTurtles
     */
    public function testCreateAnimals($AnimalOwnersCats, $AnimalOwnersDogs, $AnimalOwnersTurtles)
    {
        $cat = $AnimalOwnersCats->giveOut();
        $this->assertEquals('Miss', $cat->name);
        $this->assertEquals(14, $cat->age);
        $this->assertEquals('1 год и 2 месяца', $cat->ageFormated);
        $this->assertEquals('1 год и 2 месяца', $cat->getAgeFormated());
        $dog = $AnimalOwnersDogs->giveOut();
        $this->assertEquals('Alf', $dog->name);
        $this->assertEquals(10, $dog->age);
        $this->assertEquals('10 месяцев', $dog->ageFormated);
        $this->assertEquals('10 месяцев', $dog->getAgeFormated());
        $turtle = $AnimalOwnersTurtles->giveOut();
        $this->assertEquals('Ferrary', $turtle->name);
        $this->assertEquals(600, $turtle->age);
        $this->assertEquals('50 лет', $turtle->ageFormated);
        $this->assertEquals('50 лет', $turtle->getAgeFormated());
        return [$cat, $dog, $turtle];
    }
    
    /**
     * 
     * @expectedException InvalidArgumentException
     */
    public function testInvalidArgument()
    {
        $cat = new AnimalOwnersCats(" ", 1);
        $dog = new AnimalOwnersCats("s", 0);
    }
}
