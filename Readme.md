﻿# ЦЕЛЬЮ ЗАДАЧИ БЫЛО ПОКАЗАТЬ ВОЗМОЖНЫЕ ВАРИАНТЫ ИСПОЛЬЗОВАНИЯ ПАТТЕРНОВ ПРОГРАММИРОВАНИЯ

## Приют животных.

Реализовать набор классов для управления приютом для животных. Приют может содержать три типа животных – кошки, собаки, черепахи. Для каждого животного при попадании в приют указывается кличка и определяется возраст,
в дальнейшем эти данные не меняются, человек, забирающий животное не может все это изменить.

 Доступны следующие операции:

 * Поместить в приют.

 * Посмотреть всех животных определенного типа, сортированных по кличке в алфавитном порядке.

 * Передать человеку  животное (определенного типа), находящееся в приюте наибольшее время.

 * Передать человеку животное (без указания типа), находящееся  приюте наибольшее время.


# THE PURPOSE OF THE TASK WAS TO SHOW POSSIBLE OPTIONS FOR USING THE PROGRAMMING PATTERNS

## Animal shelter.

Implement a set of classes for managing an animal shelter. The shelter can house three types of animals - cats, dogs, turtles. For each animal, when it enters the shelter, a nickname is indicated and age is determined, in the future these data do not change, the person taking the animal cannot change all this.

The following operations are available:

* Place in a shelter.

* View all animals of a specific type, sorted alphabetically by name.

* Give a person an animal (of a certain type) that has been in the shelter for the longest time.

* Give a person an animal (without specifying the type) that is at the shelter for the longest time.